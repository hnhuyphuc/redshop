<?php
/**
 * Created by PhpStorm.
 * User: n16dc
 * Date: 14/08/2020
 * Time: 8:28 CH
 */

class ProductPage
{
    /**
     * @var string
     */
    public static $redShopLink = "//li[@id='plg_quickicon_redshop']//a";

    /**
     * @var string
     */
    public static $productMenuLink = "//body/div[@id='redSHOPAdminContainer']/div/aside/section/ul/li[2]/a[1]";

    /**
     * @var string
     */
    public static $productManagementMenuLink = "//li[2]//ul[1]//li[1]//a[1]";

    /**
     * @var string
     */
    public static $newButton = ".btn-success";

    /**
     * @var string
     */
    public static $editButton = ".button-edit";

    /**
     * @var string
     */
    public static $deleteButton = "#//div[4]//button[1]";

    /**
     * @var string
     */
    public static $searchBox = "#keyword";

    /**
     * @var string
     */
    public static $nameDropdown = "#s2id_search_field";

    /**
     * @var string
     */
    public static $resetCountLb = "#jform_resetCount-lbl";

    /**
     * @var string
     */
    public static $sendMailYes = "//label[@for='jform_sendEmail0']";

    /**
     * @var string
     */
    public static $jsWindow = 'window.scrollTo(0,0);';

    /**
     * @var string
     */
    public static $assignedUserGroupsTab = "//ul[@id=\"myTabTabs\"]/li[2]/a";

    /**
     * @var string
     */
    public static $basicSettingsTab = "//ul[@id=\"myTabTabs\"]/li[3]";

    /**
     * @var string
     */
    public static $publicGroups = "//input[@id=\"1group_1\"]";

    /**
     * @var string
     */
    public static $backendTemplateStyleId = "#jform_params_admin_style_chzn";

    /**
     * @var string
     */
    public static $hathorDefaultStyle = "//li[contains(text(),'Hathor - Default')]";

    /**
     * @var string
     */
    public static $saveSuccessMessage = "User saved.";

    /**
     * @var string
     */
    public static $delete ="//div[@id='toolbar-delete']//button[1]";

    /**
     * @var string
     */
    public static $deleteSuccessMessage ="deleted.";

    /**
     * @var string
     */
    public static $accountTab = "//a[contains(text(),'Account Details')]";
}